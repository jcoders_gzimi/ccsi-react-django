from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    
class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )
    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)



class IssueCommentAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{'fields': ['issue','user']}),
		('Comment',{'fields':['text','file']}),
	]
	list_display=('issue','user','upload_time')

class LikesInLine(admin.TabularInline):
    model = UserLikes
    list_display=('user')
    extra = 1

class BlogCommentAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{'fields': ['blog','user']}),
		('Comment',{'fields':['text']}),
	]
	list_display=('blog','user','upload_time')

class BlogAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{'fields':['title','description','category']}),
		(None,{'fields':['photo']}),
		('Uploaded by:',{'fields':['user']}),
	]
	list_display=('title','upload_time','user')

class IssueAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{'fields':['title','description']}),
		('Issue file',{'fields':['file']}),
		('Issue photo', {'fields': ['photo']}),
		('Uploaded by:',{'fields':['user']}),
	]
	inlines = [LikesInLine]
	list_display=('title','upload_time')

admin.site.unregister(User)
admin.site.register(User,CustomUserAdmin)
admin.site.register(Blog,BlogAdmin)
admin.site.register(BlogComment,BlogCommentAdmin)
admin.site.register(Category)
admin.site.register(Ad)
admin.site.register(Issue,IssueAdmin)
admin.site.register(IssueComment,IssueCommentAdmin)
admin.site.register(UserLikes)