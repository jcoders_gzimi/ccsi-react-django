from rest_framework import serializers
from articles.models import *
from django.contrib.auth.models import User

#serializers
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username','email','profile','id')

class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.id')
    username = serializers.ReadOnlyField(source='user.username')
    email = serializers.ReadOnlyField(source='user.email')
    class Meta:
        model = Profile
        fields = ('user','photo','username','email')

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model=Blog
        fields=('id','title','description','photo','upload_time','category','user','username','blogcomment_set')
        read_only_fields=('user',)

class AdSerializer(serializers.ModelSerializer):
    class Meta:
        model=Ad
        fields=('id','description','photo','link')
          
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields=('id','name',)

class IssueSerializer(serializers.ModelSerializer):
    class Meta: 
        model=Issue
        fields=('id','title','description','file', 'photo','upload_time','user','username','issuecomment_set')
        read_only_fields=('user',)


class UserLikesSerializer(serializers.ModelSerializer):
    class Meta:
        model=UserLikes
        fields=('id','user','issue','username')
        read_only_fields=('user',)

class BlogCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model=BlogComment
        fields=('id','blog','text','user','upload_time','username','photo')
        read_only_fields=('user',)

class IssueCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model=IssueComment
        fields=('id','issue','text','file','user','upload_time','username','photo')
        read_only_fields=('user',)

class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = about
        fields= ('id','photo','user','status','description')
