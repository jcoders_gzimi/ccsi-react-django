from django.urls import path
from rest_framework import routers
from .views import *
#from django.contrib.auth import views as auth_views
from rest_auth.views import (LogoutView, PasswordResetView, PasswordResetConfirmView)
from django.conf.urls import url
from django.contrib.auth import views as auth_views

router = routers.SimpleRouter()
router.register("blog", BlogViewSet)
router.register("issue", IssueViewSet)
router.register("category", CategoryViewSet)
router.register("bcomment", BlogCommentViewSet)
router.register("icomment", IssueCommentViewSet)
router.register("likes", LikeViewSet)
router.register("users", UserViewSet)
router.register("profiles", ProfileViewSet)
router.register("about",AboutViewSet)
urlpatterns = [
    path('login/',Login),
    path('register/',register),
    path('ads/',show_ads),
    path('isadmin/',is_admin),
    path('deleteblogcomment/<com_id>/',delete_comment),
    path('deleteissuecomment/<com_id>/',delete_i_comment),
    #path("logout/",auth_views.LogoutView.as_view(),name="logout"),
    path("logout/",LogoutView.as_view(), name="logout"),
    path('changepassword/',ChangePasswordView.as_view()),
    path('deleteaccount/',delete_account),
    path("password/reset/", PasswordResetView.as_view(), name='password_reset'),
    path("password/reset/confirm/<uidb64>/<token>/", PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('currentuser',getcurrentuserid),
    path('addlike', add_issue_like),
    path('get_likes', get_likes),
    path('get_user_likes',get_user_likes),
    path('delete_like',remove_issue_like),
    path('has_liked', has_liked)
]

urlpatterns += router.urls