from rest_framework.generics import *
from django_filters.rest_framework import DjangoFilterBackend
from articles.models import *
import json
from articles.permissions import *
from .serializers import *
from django.utils.decorators import method_decorator
from rest_framework import renderers
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes,action
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import filters
from rest_framework import viewsets
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions,status
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.status import HTTP_409_CONFLICT
from rest_framework.status import HTTP_200_OK
from rest_framework.authtoken.models import Token
import django_filters
from django.views.decorators.debug import sensitive_post_parameters
from django.db.models import Q

#
#
#
#
#
#

class UserViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAdminUser,)

@api_view(["GET"])
@permission_classes((permissions.IsAuthenticated,))
def getcurrentuserid(request):
    return Response(request.user.id)

@api_view(['GET'])
def is_admin(request):
    if request.user.is_superuser:
        return Response(True)
    else:
        return Response(False)

@api_view(["POST"])
@permission_classes((permissions.IsAuthenticated, ))
def get_likes(request):
    issue_id = request.POST.get("issue")
    print(issue_id)
    issue = Issue.objects.get(id=issue_id)
    return Response({"likes": UserLikes.objects.filter(issue=issue).count()}, status=200)

@api_view(["GET"])
@permission_classes((permissions.IsAuthenticated, ))
def get_user_likes(request):
    serializer = UserLikesSerializer(UserLikes.objects.filter(user = request.user), many=True)
    return Response({"liked_issues": serializer.data}, status=200)

@api_view(["POST"])
@permission_classes((permissions.IsAuthenticated, ))
def delete_account(request):
    User.objects.filter(id = request.user.id).delete()
    return Response(200)

@api_view(["POST"])
@permission_classes((permissions.IsAuthenticated, ))
def add_issue_like(request):
    issue = request.POST.get("issue")
    user = request.user
    found_issue = Issue.objects.get(id=issue)
    
    if(UserLikes.objects.filter(user=user, issue=found_issue).exists()):
        return Response(status = 409)
    
    ul = UserLikes(user=user, issue=found_issue)
    ul.save()
    return Response(status=200)

@api_view(["POST"])
@permission_classes((permissions.IsAuthenticated, ))
def remove_issue_like(request):
    issue = request.POST.get("issue")
    user = request.user
    found_issue = Issue.objects.get(id = issue)
    if(found_issue == None):
        return Response(status = 404)

    if(not UserLikes.objects.filter(user=user, issue=found_issue).exists()):
        return Response(status = 404)
    
    UserLikes.objects.filter(user = user, issue=found_issue).delete()
    return Response(status = 204)

@api_view(["POST"])
@permission_classes((permissions.IsAuthenticated, ))
def has_liked(request):
    issue = request.POST.get("issue")
    found_issue = Issue.objects.get(id = issue)
    if(found_issue == None):
         return Response(status = 404)
    user = request.user
    return Response({"hasLiked": UserLikes.objects.filter(user = user, issue=found_issue).exists()})

class ProfileViewSet(mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     viewsets.GenericViewSet):
    queryset = Profile.objects.order_by('user')
    serializer_class = ProfileSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    @action(methods=['get'], detail=True)
    def favoriteissues(self, request,*args,**kwargs):
        
        queryset = Issue.objects.filter(userlikes__user=request.user)
        serializer = IssueSerializer(queryset, many=True)
        return Response(serializer.data)
        
    
#login, register, change password, delete user account
class ChangePasswordView(UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_401_UNAUTHORIZED)
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response("Success.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DeleteAccountView(DestroyAPIView):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    model = User
    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

@api_view(["POST"])
@permission_classes((permissions.AllowAny,))
def Login(request):
    email = request.data.get("email")
    password = request.data.get("password")
    username = User.objects.get(email=email.lower()).username
    user = authenticate(username=username, password=password)
    if not user:
       return Response({"error": "Wrong username/password."}, status=HTTP_401_UNAUTHORIZED)
    login(request,user)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key},status=status.HTTP_200_OK)

@api_view(["POST"])
@permission_classes((permissions.AllowAny,))
def register(request):
    username=request.data.get("username")
    password=request.data.get("password")
    email=request.data.get("email")
    if(len(password)<8):
        return Response({'error':'Password should have at least 8 characters.'}, status=400)
    if(request.user.is_authenticated):
        return Response({
            'alreadyAuthenticated': True,
            'error': 'You are already authenticated.'
        }, status=HTTP_409_CONFLICT)
    if(User.objects.filter(email=email).exists()):
        return Response({'error': 'An account by that email/username already exists.'}, status = HTTP_409_CONFLICT)
    user = User.objects.create_user(username,email)
    user.set_password(password)
    user.save()
    login(request._request,user)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key},status=status.HTTP_200_OK)
    

@api_view(["POST"])
@permission_classes((permissions.AllowAny,))
def search_Blog(request):
    fil_category=request.POST.getlist('category')
    name=request.POST['name'].get()
    queryset = Blog.objects.filter(cateory_id__in=fil_category|Q(title__iexact=name))
    serializer= BlogSerializer(queryset,many=True)
    #return Response(status=status.HTTP_200_OK)
    return Response({'blogse':serializer},status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes((permissions.AllowAny,))
def serch_Issue(request):
    title=request.POST['title'].get()
    queryset = Issue.objects.filter(title__iexact=title)
    serializer= IssueSerializer(queryset,many=True)
    #return Response(status=status.HTTP_200_OK)
    return Response({'issue':serializer},status=status.HTTP_200_OK)



@api_view(["GET"])
@permission_classes((permissions.AllowAny,))
def show_ads(request):
    queryset = Ad.objects.all()
    serializer = AdSerializer(queryset,many=True)
    return Response(serializer.data)
#
#
#
#
#
#
#month filtering class for blogs


class BlogFilter(django_filters.FilterSet):
    filter_month = django_filters.NumberFilter(field_name='upload_time',lookup_expr='month')
    class Meta:
        model = Blog
        fields = ['filter_month']
#
#
#
#
#
#
#viewsets
class BlogViewSet(ModelViewSet):
    queryset = Blog.objects.order_by('-upload_time')
    serializer_class=BlogSerializer
    permission_classes=[
        permissions.IsAuthenticatedOrReadOnly
    ]
    filter_backends = (filters.SearchFilter,DjangoFilterBackend)
    search_fields = ('title','description')
    filterset_class=BlogFilter
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
    @action(methods=['get'], detail=False)
    def latest_blogs(self, request):
        queryset = Blog.objects.order_by('-upload_time')[:3]
        serializer = BlogSerializer(queryset, many=True)
        return Response(serializer.data)
    @action(methods=['get'], detail=True)
    def comments(self, request,*args,**kwargs):
        blog = self.get_object()
        queryset = BlogComment.objects.filter(blog=blog)
        serializer = BlogCommentSerializer(queryset, many=True)
        return Response(serializer.data)
    def update(self,request,*args,**kwargs):
        partial = kwargs.pop('partial',False)
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        serializer = self.get_serializer(instance,data=request.data,partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
        
        




class IssueViewSet(ModelViewSet):
    queryset = Issue.objects.order_by('-upload_time')
    serializer_class=IssueSerializer
    permission_classes=[
        permissions.IsAuthenticated
    ]
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title','description')
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
    def update(self,request,*args,**kwargs):
        partial = kwargs.pop('partial',False)
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        serializer = self.get_serializer(instance,data=request.data,partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
    @action(methods=['get'], detail=True)
    def liked_by(self, request,*args,**kwargs):
        issue = self.get_object()
        queryset = UserLikes.objects.filter(issue=issue)
        #queryset = UserLikes.objects.filter(issue=issue).user.username
        users_who_liked = []
        for i in queryset:
            users_who_liked.append(i.user.username)
        return Response(users_who_liked)
        
    @action(methods=['get'], detail=True)
    def likes(self, request,*args,**kwargs):
        issue = self.get_object()
        queryset = UserLikes.objects.filter(issue=issue)
        number_of_likes = queryset.count()
        return Response(number_of_likes)
    @action(methods=['get'], detail=True)
    def comments(self, request,*args,**kwargs):
        issue = self.get_object()
        queryset = IssueComment.objects.filter(issue=issue)
        serializer = IssueCommentSerializer(queryset, many=True)
        return Response(serializer.data)

@api_view(["POST"])
@permission_classes((permissions.IsAdminUser, ))
def delete_comment(request,com_id):
    BlogComment.objects.filter(id=com_id).delete()
    return Response(200)

@api_view(["POST"])
@permission_classes((permissions.IsAdminUser, ))
def delete_i_comment(request,com_id):
    IssueComment.objects.filter(id=com_id).delete()
    return Response(200)

class CategoryViewSet(ModelViewSet,mixins.CreateModelMixin):
    queryset = Category.objects.all()
    serializer_class=CategorySerializer
    permission_classes=[
        permissions.AllowAny, 
    ]

class IssueCommentViewSet(mixins.DestroyModelMixin,mixins.RetrieveModelMixin,mixins.CreateModelMixin,viewsets.GenericViewSet):
    queryset = IssueComment.objects.order_by('-upload_time')
    serializer_class=IssueCommentSerializer
    permission_classes=[
        permissions.IsAuthenticated
    ]
    #authentication_classes = (TokenAuthentication,)
    filter_backends = (DjangoFilterBackend,filters.SearchFilter)
    filterset_fields = ('issue',)
    search_fields = ('user__username', 'issue__title','text')
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

class BlogCommentViewSet(mixins.DestroyModelMixin,mixins.RetrieveModelMixin,mixins.CreateModelMixin,viewsets.GenericViewSet):
    queryset = BlogComment.objects.order_by('-upload_time')
    serializer_class=BlogCommentSerializer
    permission_classes=[
        permissions.IsAuthenticatedOrReadOnly
    ]
    filter_backends = (DjangoFilterBackend,filters.SearchFilter)
    filterset_fields = ('blog',)
    search_fields = ('user__username', 'blog__title','text')
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
        
class LikeViewSet(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.DestroyModelMixin,viewsets.GenericViewSet):
    queryset = UserLikes.objects.all()
    serializer_class=UserLikesSerializer
    permission_classes=[
        permissions.IsAuthenticated
    ]
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('issue__title',)
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.user != request.user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class AboutViewSet(mixins.DestroyModelMixin,mixins.RetrieveModelMixin,mixins.CreateModelMixin,viewsets.GenericViewSet):
    queryset= about.objects.all()
    permission_classes=[
        permissions.IsAuthenticatedOrReadOnly
        ]   
    serializer_class=AboutSerializer

