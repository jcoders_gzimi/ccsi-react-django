from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to="profilepics/",null=False, blank=False, default='/profilepics/photo.jpg') 

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Categories"
   
class Blog(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=250)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    description = models.TextField()
    photo = models.ImageField(upload_to="blogpics/")
    upload_time = models.DateTimeField(auto_now_add=True)
    category = models.ManyToManyField(Category)
    @property
    def username(self):
        return self.user.username
    def __str__(self):  
        return self.title
    def datepublished(self):
        return self.pub_date.strftime('%B %d %Y')

class Issue(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    title = models.CharField(max_length=250)
    description = models.TextField()
    file = models.FileField(upload_to='uploaded_files/', null=True, blank=True, verbose_name="")
    photo = models.FileField(upload_to='uploaded_photos/', null=True, blank=True, verbose_name="")
    upload_time = models.DateTimeField(auto_now_add=True)
    @property
    def username(self):
        return self.user.username
    def __str__(self):
        return self.title

class Ad(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField()
    photo = models.ImageField(upload_to="ads")
    link = models.CharField(max_length=1000)
    def __str__(self):
        return self.description

class UserLikes(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    issue = models.ForeignKey(Issue, on_delete = models.CASCADE)
    def __str__(self):
        return self.user.username
    class Meta:
        verbose_name_plural = "Likes"
    @property
    def username(self):
        return self.user.username

class BlogComment(models.Model):
    id = models.AutoField(primary_key=True)
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    text = models.TextField()
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    upload_time = models.DateTimeField(auto_now_add=True)
    @property
    def username(self):
        return self.user.username
    def photo(self):
        _photo=Profile.objects.filter(user=self.user).first()
        return _photo.photo.url
    def __str__(self):
        return "Blog: "+self.blog.title

class IssueComment(models.Model):
    id = models.AutoField(primary_key=True)
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)
    file = models.FileField(upload_to='issue_comment_files/', null=True, blank=True, verbose_name="")
    text = models.TextField()
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    upload_time = models.DateTimeField(auto_now_add=True)
    @property
    def username(self):
        return self.user.username
    def photo(self):
        _photo=Profile.objects.filter(user=self.user).first()
        return _photo.photo.url
    def __str__(self):
        return "Issue: "+self.issue.title


class about(models.Model):
    photo = models.ImageField(upload_to='about_pics')
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    status = models.CharField(max_length=100)
    description = models.TextField()
    def __str__(self):
        return self.description