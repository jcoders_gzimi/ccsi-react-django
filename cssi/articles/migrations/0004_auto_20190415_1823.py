# Generated by Django 2.1.7 on 2019-04-15 16:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_auto_20190329_1809'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name_plural': 'Categories'},
        ),
        migrations.AlterModelOptions(
            name='userlikes',
            options={'verbose_name_plural': 'Likes'},
        ),
        migrations.RenameField(
            model_name='blog',
            old_name='userID',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='blogcomment',
            old_name='blogID',
            new_name='blog',
        ),
        migrations.RenameField(
            model_name='blogcomment',
            old_name='userID',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='issue',
            old_name='userID',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='issuecomment',
            old_name='issueID',
            new_name='issue',
        ),
        migrations.RenameField(
            model_name='issuecomment',
            old_name='userID',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='userlikes',
            old_name='issueID',
            new_name='issue',
        ),
        migrations.RenameField(
            model_name='userlikes',
            old_name='userID',
            new_name='user',
        ),
        migrations.RemoveField(
            model_name='issue',
            name='likes',
        ),
    ]
