# Generated by Django 2.1.5 on 2019-04-26 15:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_auto_20190415_1823'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('description', models.TextField()),
                ('photo', models.ImageField(upload_to='ads')),
                ('link', models.CharField(max_length=1000)),
            ],
        ),
    ]
