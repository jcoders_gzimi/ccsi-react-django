import React, { Component } from 'react'
import IssueCard from './components/issuecard'
import Navigation, { NavigationItem, FancyItem, BOTH, NO_AUTH_ONLY, AUTH_ONLY ,ADMIN_ONLY} from '../../components/Navigation';
import { getLatestIssues, getPages } from "../../api/issue";
import { Redirect } from "react-router-dom";
import style from "./css/issues.module.css"
import AdminComponent from '../../components/Admin'

export default class Issues extends Component {
    constructor(props) {
		super(props);
		this.state = {
			issue: {},
			filtered: {},
			currentData: [],
			pages: [],
			filtering: false,
			page: 0,
			error: false,
			categories: {},
			filteredCategories: [],
		}
	}

	componentWillMount = () => {
		getLatestIssues().then(result => {
			let data = result.data;
			this.setState({
				issue: data,
			});
			let pages = getPages(Object.values(this.state.issue), 5);
			let current = pages[this.state.page];
			this.setState({
				pages,
				currentData: current,
			});
		}).catch(err => this.setState({ err: true }));
	}

	onInputChange = (e) => {
		if (e.target.value === "") {
			this.setState({
				filtering: false
			});
			return;
		}
		let filtered = Object.keys(this.state.issue).filter(issue => this.state.issue[issue].title.indexOf(e.target.value) !== -1 || this.state.issue[issue].description.indexOf(e.target.value) !== -1);
		this.setState({
			page: 0,
			filtering: true,
			filtered
		})
	}

	setPage = (page) => {


		this.setState({
			currentData: this.state.pages[page],
			page: page
		})
	}

	populateNumbers = () => {
		let size = this.state.pages.length;
		let pages = [];
		if (this.state.page >= 1) {
			pages.push(<button key={1} aria-label="Previus" className="btn" style={{backgroundColor:"#DEDEDE"}} onClick={() => this.setPage(this.state.page - 1)}>
				<span aria-hidden="true">&laquo;</span>
        		<span class="sr-only">Previous</span>
			</button>)
			window.scrollTo(0, 0)
		}
		pages.push(<button key={2} className="btn" style={{backgroundColor:"#DEDEDE"}} disabled={true}>{this.state.page + 1}</button>)
		if (size !== this.state.page + 1) {
			pages.push(<button key={3} aria-label="Next" className="btn" style={{backgroundColor:"#DEDEDE"}} onClick={() => this.setPage(this.state.page + 1)}>
			<span aria-hidden="true">&raquo;</span>
			<span class="sr-only">Next</span></button>)
			window.scrollTo(0, 0)
		}

		return pages;
	}
	populateCategories = () => {
		let buttons = [];
		buttons.push(<button key={"general"} className="dropdown-item" onClick={() => this.handleCategoryFilter(-1)} style={{ width: "100%", borderRadius: "0px", fontSize: "15px", wordWrap: "break-word", overflowX: "auto", boxShadow: "0px 0px" }} id="butt">General</button>)
		Object.values(this.state.categories).forEach(value => {
			buttons.push(<button key={value.id} className="dropdown-item" onClick={() => this.handleCategoryFilter(value.id)} style={{ width: "100%", borderRadius: "0px", fontSize: "15px", wordWrap: "break-word", overflowX: "auto", boxShadow: "0px 0px" }} id="butt">{value.name}</button>)
		})
		return buttons;
	}
	handleCategoryFilter = index => {
		if (index === -1) {
			let filtered = Object.keys(this.state.blog)
			this.setState({
				page: 0,
				filtering: true,
				filtered
			});
			return;
		}
		if (this.state.filteredCategories.includes(index)) {
			return;
		} else {
			let filtered = Object.keys(this.state.blog).filter(blog => this.state.blog[blog].category.includes(index));
			this.setState({
				page: 0,
				filtering: true,
				filtered
			})
		}
	}
    render() {
		if(this.state.currentData === undefined) {
			return <Redirect to="/"/>
		}
        return (
            <React.Fragment>
				<Navigation>
					<NavigationItem link="/" type={BOTH}>Home</NavigationItem>
                    <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
					<NavigationItem link="/Blogs" type={AUTH_ONLY}>Blogs</NavigationItem>
					<NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
					<NavigationItem link="/signup" type={NO_AUTH_ONLY}>Sign Up</NavigationItem>
					<FancyItem link="/login" type={NO_AUTH_ONLY}>Login</FancyItem>
					<NavigationItem link="/profile" type={AUTH_ONLY}>Profile</NavigationItem>
          			<FancyItem link="/signout" type={AUTH_ONLY}>Sign Out</FancyItem>
				</Navigation>
				<div className='container'>
					<div className={`row ${style.big}`}>
						<input onInputCapture={this.onInputChange} type="text" className={`form-control col-lg-12 ${style.inputbar}`} id="inputbar" placeholder="Search" />
					
						<div className="col-lg-12">
							<div className="row mb-5">
								{
									!this.state.filtering ?
										this.state.currentData.map(issue => {
											return <IssueCard className="col-lg-12" key={issue.id} image={issue.photo} description={issue.description} id={issue.id} date={issue.upload_time ? issue.upload_time.split("T")[0] : ''} title={issue.title} />
										}) : this.state.filtered.map(key => {
											return <IssueCard className="col-lg-12" key={this.state.issue[key].id} image={this.state.issue[key].photo} description={this.state.issue[key].description} id={this.state.issue[key].id} date={this.state.issue[key].upload_time ? this.state.issue[key].upload_time.split("T")[0] : ''} title={this.state.issue[key].title} />
										})
								}
							</div>
						</div>
						<div className="col-lg-2 offset-lg-5" style={{marginBottom:"8.5%"}}>
								{this.populateNumbers()}
						</div>
					</div>
				</div>
			</React.Fragment>
        )
    }
}

