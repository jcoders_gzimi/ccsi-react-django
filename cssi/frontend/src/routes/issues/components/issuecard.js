import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import style from './css/issuecard.module.css';

export default class BlogCard extends Component {
    render() {
        return (
            <div className='col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12' id={style.blog_card}>
                <div id={style.card}>
                    <b id={style.blog_title}>{this.props.title}</b>
                    <i id={style.date}>{this.props.date}</i>
                    <p id={style.paragraph} style={{overflowX:"hidden"}}>{this.props.description}</p>
                    <div className='text-right'>
                        <Link to={"/issuepost/"+ this.props.id}>
                            <button className="btn" id={style.button}>Read More</button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}
