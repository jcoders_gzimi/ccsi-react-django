import React, { Component } from 'react';
import PhotoSide from './components/PhotoSide';
import TextSide from './components/TextSide';
import Comment from './components/comment';
import style from "./components/CSS/BlogPost_CSS.module.css";
import Navigation, { NavigationItem, FancyItem, BOTH,AUTH_ONLY,NO_AUTH_ONLY } from '../../components/Navigation';
import {getBlogById, getBlogComments, postBlogComment } from "../../api/blog";
import AdminComponent from '../../components/Admin';
import { isAdmin } from "../../api/auth"

export default class BlogPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blog: {},
            comments: [],
            poster: {},
            error: false,
            comment: '',
            admin:{},
        }
    }
    componentDidMount = () => {
        getBlogById(this.props.match.params.id).then(result => {
            let data = result.data;
            this.setState({
                blog: data
            })
            window.scrollTo(0, 0)
        }).catch(err => this.setState({ err: true }));
        getBlogComments(this.props.match.params.id).then(result => {
            let data = result.data;
            this.setState({
                comments: data
            })
        })
        isAdmin().then(result => {
            this.setState({
              admin: result
            })
          })
    }
    postComment = (e) => {
        var data = new FormData();
        data.append('blog', this.state.blog.id)
        data.append('text', this.state.comment)
        postBlogComment(data).then(res => {
            if (res.status >= 200) {
                getBlogComments(this.props.match.params.id).then(result => {
                    let data = result.data;
                    this.setState({
                        comments: data
                    })
                })
            }
        })
        this.setState({ comment: '' })
        e.preventDefault();
    }
    onTextChange = (e) => {
        this.setState({
            comment: e.target.value
        })
    }
    render() {
        console.log(this.state.comments)
        return (
            <div>
                <Navigation>
                    <NavigationItem link="/" type={BOTH}>Home</NavigationItem>
                    <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
                    <NavigationItem link="/blogs" type={BOTH}>Blog</NavigationItem>
                    <NavigationItem link="/issues" type={AUTH_ONLY}>Challenges</NavigationItem>
                    <NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
                    <NavigationItem link="/signup" type={NO_AUTH_ONLY}>Sign Up</NavigationItem>
                    <FancyItem link="/login" type={NO_AUTH_ONLY}>Login</FancyItem>
					<NavigationItem link="/profile" type={AUTH_ONLY}>Profile</NavigationItem>
                    <FancyItem link="/signout" type={AUTH_ONLY}>Sign Out</FancyItem>
                </Navigation>
                <div className="container-fluid">
                    <div className="row" id={style.main}>
                        <PhotoSide image={this.state.blog.photo} />
                        
                        <TextSide desc={this.state.blog.description} id={this.state.blog.id} date={this.state.blog.upload_time ? this.state.blog.upload_time.split("T")[0] : ''} title={this.state.blog.title}/>
                        <div className="offset-lg-3 col-lg-8 mb-5">
                        {
                            Object.keys(this.state.comments).map(key => {
                                let comment = this.state.comments[key];
                                return <Comment key={key} id={comment.id} image={comment.photo} admin={this.state.admin} date={comment.upload_time ? comment.upload_time.split("T")[0] : ''} name={comment.username} className="col-lg-12 col-md-12 col-sm-12 col-xs-12" desc={comment.text}></Comment>
                            })
                        }
                        {
                            localStorage.getItem("token") ?
                                <form onSubmit={this.postComment} style={{ height: "16%",marginBottom:"5%" }} className="row">
                                    <input type="text" value={this.state.comment} onChange={this.onTextChange} style={{ marginTop: "5%", marginLeft: "5%", width: "79%", height: "7vh", borderBottomLeftRadius: "999px", borderTopLeftRadius: "999px", color: "black", backgroundColor: "#DEDEDE", fontSize: "20px", border: "none", paddingLeft: "2%" }} id="text" placeholder="Write a Comment"></input>
                                    <button type="submit" style={{ width: "9%", marginTop: "5%",boxShadow:"0 0", backgroundColor: "#DEDEDE", borderBottomRightRadius: "999px",borderLeft:"#394155 2px solid", borderTopRightRadius: "999px", marginRight: "2%", height: "7vh" }} className="btn">Send</button>
                                </form> : ""
                        }

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}