import React, { Component } from 'react';
import style from "./CSS/BlogPost_CSS.module.css";

export default class Text extends Component {
    render() {
        return (
            <div className="col-lg-3 col-md-3 col-sm-12">
                <div id={style.imgDiv}>
                    <img src={this.props.image} alt="Profile" id={style.photo}></img>
                </div>
            </div>
        )
    }
}
