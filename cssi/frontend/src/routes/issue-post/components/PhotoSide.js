import React, { Component } from 'react';
import style from "./CSS/IssuePost_CSS.module.css";
import Image from "../../../images/heart.png";
import Image2 from "../../../images/heart_y.png";
import { postIssueLike, getLikesOfIssue, unlikeIssue, hasLiked } from "../../../api/issue";



export default class Text extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
        }
    }
    postLike = (e) => {
        e.preventDefault();

        if (this.state.clicked) {
            unlikeIssue(this.props.id).then(res => {
              getLikesOfIssue(this.props.id).then(result => {
                this.setState({
                  likes: result.data.likes
                })
              })
            })
          }
          else {
            postIssueLike(this.props.id).then(res => {
              getLikesOfIssue(this.props.id).then(result => {
                this.setState({
                  likes: result.data.likes
                })
              })
            })
          }
          this.setState({ clicked: !this.state.clicked })
    }
    render() {
        return (
            <div className="col-lg-3 col-md-3 col-sm-12">
                <div id={style.imgDiv} className="row">
                    <img src={this.props.image} alt="Profile" className="col-lg-12" id={style.photo}></img>
                    <p className="col-lg-12">This issue has {this.props.likes === 0 ? "no" : this.props.likes}  {this.props.likes > 1 || this.props.likes === 0 ? "likes" : "like"}</p>
                    <button id="butt" className="col-lg-2 btn" type="submit" onClick={this.postLike} style={{fontSize: "20px", color: "black", height: "6vh", paddingTop: "5px", borderRadius: "50%", backgroundColor: "transparent", boxShadow: "0 0", padding: "0" }}><img style={{ height: "100%", width: "100%", borderRadius: "50%" }} src={!this.state.clicked ? Image : Image2} /></button>
                </div>
            </div>
        )
    }
}
