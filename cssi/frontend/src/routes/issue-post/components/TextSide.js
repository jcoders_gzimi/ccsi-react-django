import React, { Component } from 'react'
import style from "./CSS/IssuePost_CSS.module.css";

export default class Text extends Component {
    render() {
        return (
            <div className="col-lg-9 col-md-9 col-sm-12">

                    <p id={style.title}>{this.props.title}</p>
                    <p id={style.date}>{this.props.date}</p>
                    <p id={style.text}>{this.props.desc}</p>
            </div>
        )
    }
}