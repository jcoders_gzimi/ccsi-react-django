import React, { Component } from 'react'
import PhotoSide from './components/PhotoSide'
import TextSide from './components/TextSide'
import Comment from './components/comment'
import style from "./components/CSS/IssuePost_CSS.module.css";
import Navigation, { NavigationItem, FancyItem, BOTH, AUTH_ONLY, NO_AUTH_ONLY, ADMIN_ONLY } from '../../components/Navigation';
import { getIssueById, getIssueComments, postIssueComment, postIssueLike, getLikesOfIssue, unlikeIssue, hasLiked } from "../../api/issue";
import Image from "../../images/heart.png";
import Image2 from "../../images/heart_y.png";
import { isAdmin } from "../../api/auth";
import AdminComponent from '../../components/Admin';

export default class IssuePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      issue: {},
      comments: [],
      file: '',
      likes: 0,
      ads: {},
      clicked: false,
      error: false,
      admin: {}
    }
  }

  componentDidMount = () => {
    getIssueById(this.props.match.params.id).then(result => {
      let data = result.data;
      getLikesOfIssue(data.id).then(likes => {
        hasLiked(data.id).then((hasLiked) => {
          this.setState({
            issue: data,
            clicked: hasLiked.data.hasLiked,
            likes: likes.data.likes
          })
        });
      });
      getIssueComments(this.props.match.params.id).then(result => {
        let data = result.data;
        this.setState({
          comments: data
        })
      })
      isAdmin().then(result => {
        this.setState({
          admin: result
        })
      })
    })
    isAdmin().then(result => {
      this.setState({
        admin: result
      })
    })
  }

  postComment = (e) => {
    var data = new FormData();
    data.append('issue', this.state.issue.id)
    data.append('text', this.state.comment)
    data.append('file', this.state.file)
    if (this.state.comment != undefined) {
      postIssueComment(data).then(res => {
        if (res.status >= 200) {
          getIssueComments(this.props.match.params.id).then(result => {
            let data = result.data;
            this.setState({
              comments: data
            })
          })
        }
      })
    }
    this.setState({ comment: '' })
    e.preventDefault();
  }

  postLike = (e) => {
    e.preventDefault();

    if (this.state.clicked) {
      unlikeIssue(this.state.issue.id).then(res => {
        getLikesOfIssue(this.state.issue.id).then(result => {
          this.setState({
            likes: result.data.likes
          })
        })
      })
    }
    else {
      postIssueLike(this.state.issue.id).then(res => {
        getLikesOfIssue(this.state.issue.id).then(result => {
          this.setState({
            likes: result.data.likes
          })
        })
      })
    }
    this.setState({ clicked: !this.state.clicked })



  }

  postLike = (e) => {
    e.preventDefault();

    if (this.state.clicked) {
      unlikeIssue(this.state.issue.id).then(res => {
        getLikesOfIssue(this.state.issue.id).then(result => {
          this.setState({
            likes: result.data.likes
          })
        })
      })
    }
    else {
      postIssueLike(this.state.issue.id).then(res => {
        getLikesOfIssue(this.state.issue.id).then(result => {
          this.setState({
            likes: result.data.likes
          })
        })
      })
    }
    this.setState({ clicked: !this.state.clicked })
  }

  onTextChange = (e) => {
    this.setState({
      comment: e.target.value
    })
  }
  onChangeHandler = event => {
    this.setState({
      file: event.target.files[0],
      loaded: 0,
    })
  }

  render() {
    console.log(this.state.issue)
    return (
      <div>
        <Navigation>
          <NavigationItem link="/" type={BOTH}>Home</NavigationItem>
          <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
          <NavigationItem link="/blogs" type={BOTH}>Blogs</NavigationItem>
          <NavigationItem link="/issues" type={AUTH_ONLY}>Challenges</NavigationItem>
          <NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
          <NavigationItem link="/signup" type={NO_AUTH_ONLY}>Sign Up</NavigationItem>
          <FancyItem link="/login" type={NO_AUTH_ONLY}>Login</FancyItem>
          <NavigationItem link="/profile" type={AUTH_ONLY}>Profile</NavigationItem>
          <FancyItem link="/signout" type={AUTH_ONLY}>Sign Out</FancyItem>
        </Navigation>
        <div className="container-fluid">
          <div className="row" id={style.main}>
            <div className="col-lg-3 col-md-3 col-sm-12">
              <div id={style.imgDiv} className="row">
                <img src={this.state.issue.photo} alt="Profile" className="col-lg-12" id={style.photo}></img>
                <div className="col-lg-12" style={{ marginTop: "5%" }}>
                  <div className="row">
                    <button id="butt" className="col-lg-2 col-xl-2 col-md-2 col-sm-2 col-2 btn" type="submit" onClick={this.postLike} style={{ fontSize: "20px", color: "black", height: "6vh", paddingTop: "5px", borderRadius: "50%", backgroundColor: "transparent", boxShadow: "0 0", padding: "0" }}><img style={{ height: "100%", width: "100%", borderRadius: "50%", maxWidth: "50px" }} src={!this.state.clicked ? Image : Image2} /></button>
                    <p className="col-lg-10 col-xl-10 col-md-10 col-sm-10 col-10" style={{ marginTop: "1.5vh" }}>{this.state.likes === 0 ? "no" : this.state.likes}  {this.state.likes > 1 || this.state.likes === 0 ? "likes" : "like"}</p>
                  </div>
                </div>
              </div>
            </div>
            <TextSide desc={this.state.issue.description} id={this.state.issue.id} date={this.state.issue.upload_time ? this.state.issue.upload_time.split("T")[0] : ''} title={this.state.issue.title} />
            <div className="offset-lg-3 col-lg-8 mb-5">
              {
                Object.keys(this.state.comments).map(key => {
                  let comment = this.state.comments[key];
                  return <Comment key={comment.id} id={comment.id} image={comment.photo} admin={this.state.admin.data} date={comment.upload_time ? comment.upload_time.split("T")[0] : ''} name={comment.username} url={comment.file} desc={comment.text} className="col-lg-12 col-md-12 col-sm-12 col-xs-12">{comment.text}</Comment>
                })
              }
              <form style={{ height: "16%" ,marginBottom:"5%"}} className="row" onSubmit={this.postComment}>
                <label className={`btn ${style.fileContainer}`}>
                  Attach file
                  <input type="file" onChange={this.onChangeHandler} style={{ width: "100%"}} className="btn"></input>

                </label>
                <input type="text" value={this.state.comment} onChange={this.onTextChange} style={{ marginTop: "5%", width: "64.5%", height: "7vh", color: "black", backgroundColor: "#DEDEDE", fontSize: "20px", border: "none", paddingLeft: "2%" }} placeholder="Write a Comment"></input>
                <button type="submit" style={{ width: "9%", marginTop: "5%", boxShadow: "0 0", backgroundColor: "#DEDEDE", borderBottomRightRadius: "999px", borderTopRightRadius: "999px", marginRight: "5%", height: "7vh",borderLeft:"#394155 2px solid" }} className="btn">Send</button>
              </form>

            </div>
          </div>
        </div>
      </div>
    )
  }
}