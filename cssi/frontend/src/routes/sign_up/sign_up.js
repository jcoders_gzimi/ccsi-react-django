import React, { Component } from 'react';
import style from './css/sign_up.module.css';
import Image from "../../images/home_pic.jfif";
import { Link, Redirect } from "react-router-dom";
import Navigation, { NavigationItem, FancyItem, BOTH, NO_AUTH_ONLY, AUTH_ONLY,ADMIN_ONLY } from '../../components/Navigation';
import { authenticateUser, registerUser } from "../../api/auth";
import Results from "../../api/results";
import AdminComponent from '../../components/Admin'

export default class signUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      email: "",
      confirm: "",
      authFail: false,
      redirect: false,
      status: <p>Wrong username/password.</p>,
    }
  }

  handleUsernameChange = (e) => {
    this.setState({
      username: e.target.value
    })
  }

  handlePasswordChange = (e) => {
    this.setState({
      password: e.target.value
    })
  }

  handleEmailChange = (e) => {
    this.setState({
      email: e.target.value
    })
  }

  handleConfirm = (e) => {
    this.setState({
      confirm: e.target.value
    })
  }


  handleSubmit = (e) => {
    if (this.state.confirm == this.state.password) {
      this.setState({ authFail: false });
      this.setState({ status: <i className="fas fa-spinner fa-spin" /> })

      registerUser(this.state.email, this.state.username, this.state.password).then(result => {
        if (result.result === Results.REGISTER_SUCCESS) {
          authenticateUser(this.state.email, this.state.password).then(result => {
            if (result.status === 200) {
              localStorage.setItem("token", result.data.token);
              this.setState({authFail: false, redirect: true});
            }
          }).catch(err => {
            this.setState({ authFail: true, redirect: false, status: "Wrong username/password." })
          });
        }
      }).catch(err => {
        this.setState({ authFail: true, redirect: false, status: <p>That account already exists.</p> })
      });
      e.preventDefault();
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.state.redirect ? <Redirect to="/" /> : <React.Fragment>
          <div className="body">
            <Navigation>
              <NavigationItem link="/" type={BOTH}>Home</NavigationItem>
              <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
              <NavigationItem link="/blogs" type={BOTH}>Blogs</NavigationItem>
              <NavigationItem link="/issues" type={AUTH_ONLY}>Challenges</NavigationItem>
              <NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
              <FancyItem link="/login" type={NO_AUTH_ONLY}>Login</FancyItem>
            </Navigation>
            <div className="container">
              <div className={`row ${style.sign_up_container}`}>
                <div className="col-lg-6">
                  <img src={Image} alt="" className={style.image} />
                </div>
                <div className="col-lg-6">
                  <h2><b>Welcome to CCSI</b></h2>
                  <p className={style.paragraph}>Donec maximus porta purus, vel tristique orci ultricies id. Pellentesque quis suscipit turpis. Aenean feugiat ante sem, ac rhoncus quam gravida vel.</p>
                  <h6>Already have an account? <Link to="/login">Log In</Link></h6>
                  <form onSubmit={this.handleSubmit}>
                    <div className="row">
                      <div className={`offset-lg-1 col-lg-5 ${style.first_inp}`}>
                        <div className="row">
                          <label className="col-lg-12">Username:</label>
                          <input type="text" className={`col-lg-12 ${style.input}`} onChange={this.handleUsernameChange} />

                          <label className="col-lg-12">Email:</label>
                          <input type="email" className={`col-lg-12 ${style.input}`} onChange={this.handleEmailChange} />

                        </div>
                      </div>
                      <div className={`offset-lg-1 col-lg-5 ${style.sec_inp}`}>
                        <div className="row">
                          <label className="col-lg-12">Password:</label>
                          <input type="password" className={`col-lg-12 ${style.input}`} onChange={this.handlePasswordChange} />

                          <label className="col-lg-12">Confirm Password:</label>
                          <input type="password" className={`col-lg-12 ${style.input}`} onChange={this.handleConfirm} />
                        </div>
                      </div>

                      {/* <input type="checkbox" className={`${style.check}`} /><p className={style.agree}>I agree to the Terms and Privacy Policy </p> */}

                    </div>
                    <div className={`text-center`}>
                      <input onClick={this.handleSubmit} type="submit" className={`btn ${style.submit}`}></input>
                    </div>
                  </form>

                  {
                    this.state.authFail ? this.state.status : ""
                  }

                </div>
              </div>
            </div>
          </div>
        </React.Fragment>}
      </React.Fragment>
    )
  }
}
