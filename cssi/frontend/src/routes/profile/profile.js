import React, { Component } from 'react';
import style from './css/profile.module.css';
import Navigation, { FancyItem, NavigationItem, BOTH, AUTH_ONLY, ADMIN_ONLY } from '../../components/Navigation';
import LikedIssue from './components/liked_issue.js';
import { getUserById, currentUser, getUserLikes, getIssueById, updateProfilePicture } from "../../api/profile";
import AdminComponent from '../../components/Admin'
import { deleteAccount } from '../../api/profile'
import { Redirect } from "react-router-dom"
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      error: false,
      image: null,
      redirect: false,
      issues: [],
    }
  }


  componentDidMount = () => {
    currentUser().then(result =>
      getUserById(result.data).then(result => {
        getUserLikes().then(issuesResult => {
          let issueIds = issuesResult.data.liked_issues.map(issue => issue.issue);
          this.mapIdsToIssues(issueIds).then((issues) => {
            this.setState({ profile: result.data, issues: issues })
          })
        })
      }));


  }
  onDelete = () => {
    deleteAccount().then(() => {
      localStorage.removeItem("token");
      this.setState({
        redirect: true
      })
    });
  }

  submitProfilePicture = (e) => {
    console.log(this.state.image)
    var data = new FormData();
    data.append('photo', this.state.image)
    updateProfilePicture(data, this.state.profile.user).then(() => {
      getUserById(this.state.profile.user).then(result => {
        this.setState({ profile: result.data });
      });
    });
    this.setState({ image: '' })
    e.preventDefault();
  }
  onInputChange = (e) => {
    this.setState({
      image: e.target.files[0]
    })
  }

  mapIdsToIssues = async (ids) => {
    return await Promise.all(ids.map(item => getIssueById(item)));
  }
  render() {
    console.log(this.state.issues)
    return (
      <div>
        {!this.state.redirect ?
          <div>
            <Navigation>
              <NavigationItem link="/" type={BOTH}>Home</NavigationItem>
              <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
              <NavigationItem link="/blogs" type={BOTH}>Blogs</NavigationItem>
              <NavigationItem link="/issues" type={AUTH_ONLY}>Challenges</NavigationItem>
              <NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
              <FancyItem link="/signout" type={AUTH_ONLY}>Sign Out</FancyItem>
            </Navigation>
            <div className={`container ${style.big} `}>
              <div className={`row`}>
                <div className={`offset-lg-1 col-lg-3 ${style.pic_container}`}>
                  <div className="text-center" style={{ fontFamily: "Roboto", fontSize: "30px" }}>{this.state.profile.username}</div>
                  <img src={this.state.profile.photo} style={{ borderRadius: "15px", maxWidth: "250px", maxHeight: "350px" }} alt="" className={style.pic} />
                  <form onSubmit={this.submitProfilePicture}>
                  <label className={`btn ${style.fileContainer} ${style.button}`}>
                  Choose New Profile Picture:
                    <input type="file" onChange={this.onInputChange}/>
                  </label>
                    <button type="submit" className={`${style.button} ${style.change}`}>Change Profile Picture</button>
                  </form>
                  <button onClick={this.onDelete} className={`${style.button}`}>Delete This Account</button>
                </div>

                <div id="issues" className={`offset-lg-2 col-lg-6`}>
                  <h1 className={`text-center`}>

                    {!this.state.issues || this.state.issues.length === 0 ? "No Liked Issues" : "Liked Issues:"}
                  </h1>
                  <p className={style.liked}>
                    {
                      Object.values(this.state.issues).map(value => {
                        let issue = value.data;
                        return (
                          <LikedIssue key={issue.id} title={issue.title} description={issue.description} date={issue.upload_time.split("T")[0]} id={issue.id} photo={issue.photo} />

                        )
                      })
                    }

                  </p>
                </div>


              </div>
            </div>
          </div>
          : <Redirect to="/" />}
      </div>
    )
  }
}
