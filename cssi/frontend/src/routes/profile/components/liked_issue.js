import React, { Component } from 'react';
import style from './css/liked_issue.module.css';
import { Link } from 'react-router-dom';

export default class LikedIssue extends Component {
    render() {
        return (
            <div>
                <div id={style.blog_card}>
                    <b id={style.blog_title}>{this.props.title}</b>
                    <p id={style.paragraph}>{this.props.description}</p>
                    <div className='text-right'>
                        <Link to={"/issuepost/" + this.props.id}>
                            <button className="btn" id={style.button}>Read More</button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}
