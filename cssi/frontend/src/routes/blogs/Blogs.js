import React, { Component } from 'react'
import BlogCard from './components/blogcard'
import Navigation, { NavigationItem, FancyItem, BOTH, NO_AUTH_ONLY, AUTH_ONLY,ADMIN_ONLY, } from '../../components/Navigation';
import { getLatestBlogs, getPages, getCategories } from "../../api/blog";
import style from "./css/Blogs.module.css";
import Footer from '../../components/Footer';
import AdminComponent from '../../components/Admin'

export default class Blogs extends Component {

	constructor(props) {
		super(props);
		this.state = {
			blog: {},
			filtered: {},
			currentData: [],
			pages: [],
			filtering: false,
			page: 0,
			error: false,
			categories: {},
			filteredCategories: [],
		}
	}

	componentWillMount = () => {
		getLatestBlogs().then(result => {
			let data = result.data;
			this.setState({
				blog: data,
			});
			let pages = getPages(Object.values(this.state.blog), 9);
			let current = pages[this.state.page];
			this.setState({
				pages,
				currentData: current,
			});
			getCategories().then(result => {
				this.setState({
					categories: result.data
				})
			})
		}).catch(err => this.setState({ err: true }));
	}

	onInputChange = (e) => {
		if (e.target.value === "") {
			this.setState({
				filtering: false
			});
			return;
		}
		let filtered = Object.keys(this.state.blog).filter(blog => this.state.blog[blog].title.indexOf(e.target.value) !== -1 || this.state.blog[blog].description.indexOf(e.target.value) !== -1);
		this.setState({
			page: 0,
			filtering: true,
			filtered
		})
	}

	setPage = (page) => {


		this.setState({
			currentData: this.state.pages[page],
			page: page
		})
	}
	

	populateNumbers = () => {
		let size = this.state.pages.length;
		let pages = [];
		if (this.state.page >= 1) {
			pages.push(<button key={1} aria-label="Previus" className="btn" style={{backgroundColor:"#DEDEDE"}} onClick={() => this.setPage(this.state.page - 1)}>
				<span aria-hidden="true">&laquo;</span>
        		<span class="sr-only">Previous</span>
			</button>)
			window.scrollTo(0, 0)
		}
		pages.push(<button key={2} className="btn" style={{backgroundColor:"#DEDEDE"}} disabled={true}>{this.state.page + 1}</button>)
		if (size !== this.state.page + 1) {
			pages.push(<button key={3} aria-label="Next" className="btn" style={{backgroundColor:"#DEDEDE"}} onClick={() => this.setPage(this.state.page + 1)}>
			<span aria-hidden="true">&raquo;</span>
			<span class="sr-only">Next</span></button>)
			window.scrollTo(0, 0)
		}

		return pages;
	}

	handleCategoryFilter = index => {
		if (index === -1) {
			let filtered = Object.keys(this.state.blog)
			this.setState({
				page: 0,
				filtering: true,
				filtered
			});
			return;
		}
		if (this.state.filteredCategories.includes(index)) {
			return;
		} else {
			let filtered = Object.keys(this.state.blog).filter(blog => this.state.blog[blog].category.includes(index));
			this.setState({
				page: 0,
				filtering: true,
				filtered
			})
		}
	}

	populateCategories = () => {
		let buttons = [];
		buttons.push(<button key={"general"} className="dropdown-item" onClick={() => this.handleCategoryFilter(-1)} style={{ width: "100%", borderRadius: "0px", fontSize: "15px", wordWrap: "break-word", overflowX: "auto", boxShadow: "0px 0px" }} id="butt">General</button>)
		Object.values(this.state.categories).forEach(value => {
			buttons.push(<button key={value.id} className="dropdown-item" onClick={() => this.handleCategoryFilter(value.id)} style={{ width: "100%", borderRadius: "0px", fontSize: "15px", wordWrap: "break-word", overflowX: "auto", boxShadow: "0px 0px" }} id="butt">{value.name}</button>)
		})
		return buttons;
	}
	render() {
		return (
			<React.Fragment>
				<Navigation>
					<NavigationItem link="/" type={BOTH}>Home</NavigationItem>
                    <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
					<NavigationItem link="/issues" type={AUTH_ONLY}>Challenges</NavigationItem>
					<NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
					<NavigationItem link="/signup" type={NO_AUTH_ONLY}>Sign Up</NavigationItem>
					<NavigationItem link="/profile" type={AUTH_ONLY}>Profile</NavigationItem>
					<FancyItem link="/login" type={NO_AUTH_ONLY}>Login</FancyItem>
          			<FancyItem link="/signout" type={AUTH_ONLY}>Sign Out</FancyItem>
				</Navigation>
				<div className='container'>
					<div className={`row ${style.big}`}>
						<input onInputCapture={this.onInputChange} type="text" className={`form-control col-lg-10	 ${style.inputbar}`} id="inputbar" placeholder="Search" />
						<div className="dropdown col-lg-2">
							<button className="btn btn-secondary col-lg-12 dropdown-toggle" data-toggle="dropdown">Categories</button>
							<div className="dropdown-menu"  aria-labelledby="dropdownMenuButton">
								{this.populateCategories()}
							</div>
						</div>
					
						<div className="col-lg-12">
							<div className="row mb-5">
								{
									!this.state.filtering ?
										this.state.currentData.map(blog => {
											return <BlogCard className="col-lg-12" key={blog.id} image={blog.photo} description={blog.description} id={blog.id} date={blog.upload_time ? blog.upload_time.split("T")[0] : ''} title={blog.title} />
										}) : this.state.filtered.map(key => {
											return <BlogCard className="col-lg-12" key={this.state.blog[key].id} image={this.state.blog[key].photo} description={this.state.blog[key].description} id={this.state.blog[key].id} date={this.state.blog[key].upload_time ? this.state.blog[key].upload_time.split("T")[0] : ''} title={this.state.blog[key].title} />
										})
								}
							</div>
						</div>
						<div className="col-lg-2 offset-lg-5" style={{marginBottom:"8.5%"}}>
								{this.populateNumbers()}
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

