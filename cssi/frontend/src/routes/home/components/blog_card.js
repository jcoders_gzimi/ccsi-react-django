import React, { Component } from "react";
import { Link } from "react-router-dom";
import style from "./css/blog_card.module.css";

export default class BlogCard extends Component {
  render() {
    return (
      <div className={`col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12`}>
        <div id={style.body}>
          <p id={`${style.title}`}> {this.props.title}</p>
          <p id={style.texti}  style={{overflowX:"hidden"}}>{this.props.desc}</p>
          <div className={`text-center`}>
            <Link to={"/blogpost/" + this.props.id}>
              <button className={`btn`} id={style.button}>
                Read More
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
