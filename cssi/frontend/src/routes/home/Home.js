import React, { Component } from 'react'
import Navigation, { NavigationItem, FancyItem, BOTH, NO_AUTH_ONLY, AUTH_ONLY } from '../../components/Navigation';
import Image from "../../images/home_pic.jfif";
import style from "./css/home.module.css";
import AdminComponent from '../../components/Admin'
import BlogCard from "./components/blog_card.js";
import { getFrontBlogs} from "../../api/home";

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shouldSignOut: false,
      ads: [],
      profile_id: {},
      latestBlogs: {},
      admin:{}
    }
  }

  componentWillMount() {
    getFrontBlogs().then(result => {
      this.setState({ latestBlogs: result.data });
    })
    

  }
  render() {
    return (
      <div>
        <Navigation>
          <NavigationItem link="/blogs" type={BOTH}>Blogs</NavigationItem>
          <AdminComponent><NavigationItem link="/admin">Admin</NavigationItem></AdminComponent>
          <NavigationItem link="/issues" type={AUTH_ONLY}>Challenges</NavigationItem>
          <NavigationItem link="/about" type={BOTH}>About Us</NavigationItem>
          <NavigationItem link="/signup" type={NO_AUTH_ONLY}>Sign Up</NavigationItem>
          <FancyItem link="/login" type={NO_AUTH_ONLY}>Login</FancyItem>
					<NavigationItem link="/profile" type={AUTH_ONLY}>Profile</NavigationItem>
          <FancyItem link="/signout" type={AUTH_ONLY}>Sign Out</FancyItem>
        </Navigation>
        <div className="container">
          <div className="row">
            <div className={`col-lg-8 ${style.image_container}`}>
              <img src={Image} alt="" className={style.image} />
            </div>
            <div className={`col-lg-4 text-center ${style.our}`}>
              <p id={style.mainT}>Our department</p>
              <p id={style.texti}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et nisl consequat mi feugiat tincidunt quis eget ipsum. Fusce maximus dictum volutpat. Cras nisi nibh, vestibulum ut ipsum eget, gravida fermentum sapien. Donec ornare, magna id lacinia venenatis, nunc sem sagittis erat, id consectetur risus velit ac arcu. Donec maximus porta purus, vel tristique orci ultricies id. Pellentesque quis suscipit turpis. Aenean feugiat ante sem, ac rhoncus quam gravida vel. Nullam sodales bibendum massa et malesuada. Etiam blandit porta pulvinar. Integer nulla quam, hendrerit sit amet dui non, scelerisque aliquet ex. Ut eleifend libero at malesuada ultricies. Nulla at sagittis quam, sed rhoncus magna. Phasellus eget eros metus .Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et nisl consequat mi feugiat tincidunt quis eget ipsum. Fusce maximus dictum volutpat. Cras nisi nibh, vestibulum ut ipsum eget, gravida fermentum sapien. Donec ornare, magna id lacinia venenatis, nunc sem sagittis erat, id consectetur risus velit ac arcu. Donec maximus porta purus, vel tristique orci ultricies id. Pellentesque quis suscipit turpis. Aenean feugiat ante sem, ac rhoncus quam gravida vel. Nullam sodales bibendum massa et malesuada. Etiam blandit porta pulvinar. Integer nulla quam, hendrerit sit amet dui non, scelerisque aliquet</p>
            </div>
          </div>
        </div>
        <div className={` container ${style.main}`}>
          <div className={`row`}>
            {Object.keys(this.state.latestBlogs).map(key => {
              let blog = this.state.latestBlogs[key];
              return <BlogCard id={blog.id} key={key} title={blog.title} desc={blog.description} date={blog.upload_time.split("T")[0]} image={blog.photo}></BlogCard>

            })
            }
          </div>
        </div>
      </div>

    )
  }
}
