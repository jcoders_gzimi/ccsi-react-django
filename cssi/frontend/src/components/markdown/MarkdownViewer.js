import React, { Component } from 'react'
import { getMarkdownAsRawHTML } from '../../api/general';
import style from "../../css/markdown/markdown_viewer.module.css";

const marked = require("marked");

export default class MarkdownViewer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: "",
            markdownText: {__html: ""}
        }
    }

    onTextChange = (e) => {
        let markdownText = getMarkdownAsRawHTML(e.target.value);
        this.setState({ text: e.target.value, markdownText });
        if(this.props.onSubmit !== undefined) {
            this.props.onSubmit(e.target.value);
        }
    }

    render() {
        return (
            <div className={style.markdownContainer}>
                <form className={style.markdownForm}>
                    <textarea placeholder="Text here." onChange={(e) => this.onTextChange(e)} />
                </form> 
                <div className={style.markdownPreview} dangerouslySetInnerHTML={this.state.markdownText} />
            </div>
        )
    }
}
