import React, { Component } from 'react'
import UserController, { User } from '../api/user';
import {isAdmin} from '../api/auth';

export default class AdminComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            admin:{}
        }
    }

    componentDidMount() {
        isAdmin().then(result => {
            this.setState({
                admin: result.data
            })
        })
    }

        render() {
            if(this.state.admin==false || localStorage.getItem("token") === null)
            {
                return ""
            }
            else
            {
            return this.props.children;
            }
        }
}