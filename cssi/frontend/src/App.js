import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './routes/home/Home';
import Blogs from './routes/blogs/Blogs';
import UndefinedRoute from './routes/404/UndefinedRoute';
import { PrivateRoute, PublicOnlyRoute, AdminOnlyRoute } from "./routes/RouteType";
import Footer from './components/Footer';
import BlogPost from './routes/blog-post/BlogPost';
import IssuePost from './routes/issue-post/IssuePost';
import logIn from './routes/log_in/log_in.js';
import signUp from './routes/sign_up/sign_up.js';
import Issues from './routes/issues/Issues.js';
import AboutUs from './routes/about-us/AboutUs.js';
import Admin from './routes/admin/Admin.js';
import Profile from './routes/profile/profile.js';
import SignOutRoute from './routes/sign_out/sign_out.js';
import MarkdownViewer from './components/markdown/MarkdownViewer';
function App() {
  return (
    
    <React.Fragment>
      <div id="main">
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/blogs" component={Blogs} />
            <Route exact path="/blogpost/:id" component={BlogPost} />
            <PublicOnlyRoute exact path="/login" component={logIn} />
            <PublicOnlyRoute exact path="/signup" component={signUp} />
            <PrivateRoute exact path="/issues" component={Issues} />
            <Route exact path="/issuepost/:id" component={IssuePost} />
            <Route exact path="/about" component={AboutUs} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/markdown" component={MarkdownViewer} />
            <PrivateRoute exact path="/signout" component={SignOutRoute} />
            <AdminOnlyRoute exact path="/admin" component={Admin} />
            <Route component={UndefinedRoute} />
          </Switch>
        </Router>
      </div>
      <Footer />
    </React.Fragment>
  );
}

export default App;
