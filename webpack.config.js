module.exports={
    entry: ["babel-polyfill", "./app/js"],
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader"
                    }
                ]
            },
            {
              test: /\.(png|jpg|gif|jfif)$/,
              use: [
                'file-loader'
              ]
            }
        ]
    }
}